package com.example.dos;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.SyncStateContract;
import android.util.Log;

import com.google.android.gms.common.internal.Constants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class GettingDeviceTokenService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String DeviceToken= FirebaseInstanceId.getInstance().getToken();
        Log.d("DEVICE TOKEN:", DeviceToken);

    }
}
